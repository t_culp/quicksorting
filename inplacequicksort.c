#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void swap(int *array, int a, int b) {
	int temp = array[a];
	array[a] = array[b];
	array[b] = temp;
}

/*
 * For beginning a sort, a = 0 and b = sizeofunsorted - 1
 * (first and last indices)
 */
 
int quicksort(int *unsorted, int a, int b) {
	int distance;
	if ((distance = (b - a)) == 0)
		return 1;
	if (distance == 1) {
		if (unsorted[a] > unsorted[b]) {
			swap(unsorted, a, b);
		}
		return 0;
	}
	int indexofpivot = (a + b)/ 2;
	int pivot = unsorted[indexofpivot];
	/*
	 * These next two variables tell the loop the offset of where to 
	 * put the high or low value
	 */
	int fromleft = 0; // for lesser than or equal to pivot
	int fromright = 0; // for greater than pivot
	int i = a;
	int indexofswap;
	while (i <= b - fromright) {
		if (unsorted[i] > pivot) {
			indexofswap = b - fromright++;
			if (i != indexofswap) {
				if (indexofpivot == indexofswap)
					indexofpivot = i;
				swap(unsorted, i, indexofswap);
			} else
				++i;
		} else if (unsorted[i] < pivot) {
			indexofswap = a + fromleft++;
			if (i != indexofswap) {
				if (indexofpivot == indexofswap)
					indexofpivot = i;
				swap(unsorted, i, indexofswap);
			} else
				++i;
		} else
			++i;
	}
	if ((indexofpivot - a) > 0)
		quicksort(unsorted, a, indexofpivot);
	if ((b - (indexofpivot + 1)) > 0)
		quicksort(unsorted, indexofpivot + 1, b);
	return 0;
}

#define SIZE_OF_ARRAY 10000
#define SIZE_OF_NUMS 10000
#define STARTING_NUMBER 0
#define NUM_OF_RUNS 10

int main() {
	srand(time(NULL));
	int *randarray = (int *)malloc(sizeof(int) * SIZE_OF_ARRAY);
	int times = 0;
	int j;
	short works;
	printf("Execution times (unsorted array size: %i, %i <=" \
		" (number magnitude) <= %i):\n", SIZE_OF_ARRAY, STARTING_NUMBER, \
		SIZE_OF_NUMS - 1 + STARTING_NUMBER);
	for (j = 0; j < NUM_OF_RUNS; ++j) {
		int i;
		for (i = 0; i < SIZE_OF_ARRAY; ++i) {
			randarray[i] = (rand() % SIZE_OF_NUMS) + STARTING_NUMBER;
		}
		time_t begin, end;
		begin = clock();
		quicksort(randarray, 0, SIZE_OF_ARRAY - 1);
		end = clock();
		works = 1;
		for (i = 1; i < SIZE_OF_ARRAY; ++i) {
			if (randarray[i] < randarray[i - 1])
				works = 0;
		}
		int time = (int)(end - begin);
		times += time;
		printf("\tRun %i: %i\t%s\n", j + 1, time, (works ? "sorted" : "sort failed"));
	}
	int average = times / NUM_OF_RUNS;
	printf("Average execution time: %i\n", average);
	free(randarray);
	return 0;
}
