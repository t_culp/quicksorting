#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int quicksort(int *unsorted, int sizeofunsorted) {
	if (sizeofunsorted < 1)
		return 0;
	int indexofpivot = sizeofunsorted / 2;
	int pivot = unsorted[indexofpivot];
	int i;
	int sizeoflesser = 0; // size of lesser than _or equal to_
	int sizeofgreater = 0;
	for (i = 0; i < sizeofunsorted; ++i) {
		if (i != indexofpivot) {
			if (unsorted[i] > pivot)
				++sizeofgreater;
			else
				++sizeoflesser;
		}
	}
	int *lesser;
	int *greater;
	if (sizeoflesser)
		lesser = (int *)malloc(sizeof(int) * sizeoflesser);
	if (sizeofgreater)
		greater = (int *)malloc(sizeof(int) * sizeofgreater);
	int indexingreater = 0;
	int indexinlesser = 0;
	for (i = 0; i < sizeofunsorted; ++i) {
		if (i != indexofpivot) {
			if (unsorted[i] > pivot)
				greater[indexingreater++] = unsorted[i];
			else
				lesser[indexinlesser++] = unsorted[i];
		}
	}
	if (sizeoflesser > 1)
		quicksort(lesser, sizeoflesser);
	if (sizeofgreater > 1)
		quicksort(greater, sizeofgreater);
	for (i = 0; i < sizeoflesser; ++i) {
		unsorted[i] = lesser[i];
	}
	unsorted[i++] = pivot;
	indexingreater = 0;
	for (i = i; i < sizeofunsorted; ++i) {
		unsorted[i] = greater[indexingreater++];
	}
	if (sizeoflesser)
		free(lesser);
	if (sizeofgreater)
		free(greater);
	return 1;
}

int main(void) {
	int sizeofarray = 100000;
	int numofruns = 10;
	int sizeofnums = 100000;
	int *times = (int *)malloc(sizeof(int) * numofruns);
	srand(time(NULL));
	int run;
	time_t begin, end;
	printf("Executions:\n");
	for (run = 0; run < numofruns; ++run) {
		int *randarray = (int *)malloc(sizeof(int) * sizeofarray);
		int i;
		for (i = 0; i < sizeofarray; ++i) {
			randarray[i] = (rand() % sizeofnums);
		}
		begin = clock();
		quicksort(randarray, sizeofarray);
		end = clock();
		/*
		for (i = 0; i < sizeofarray - 1; ++i) {
			if (randarray[i] <= randarray[i + 1])
				continue;
			else {
				printf("ERROR");
				i = sizeofarray;
			}
		}
		*/
		int time = (int)(end - begin);
		printf("\tRun %i: %i\n", run + 1, time);
		times[run] = time;
	}
	long sum = 0;
	for (run = 0; run < numofruns; ++run) {
		sum += times[run];
	}
	int average = sum / numofruns;
	printf("Average execution: %i\n", average);
	return 0;
}
